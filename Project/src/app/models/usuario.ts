export class Usuario {
    id: number;
    email: string;
    password: string;
    name: string;
    lastname: string;
    country: string;
    phone: number;
    autenticado: boolean;
    token: string;
    age: string;
    code: string;
}